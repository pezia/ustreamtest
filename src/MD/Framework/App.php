<?php

namespace MD\Framework;

use MD\Framework\Request\HttpRequest;

/**
 * Main application class
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
abstract class App {

    /**
     * @var array Route "collection"
     */
    protected $routes = array();

    /**
     * @var HttpRequest Request object
     */
    protected $request = null;

    /**
     * Setup the routes
     */
    abstract protected function setupRoutes();

    /**
     * Handles application shutdown
     * This should eg. clean up resources
     */
    protected function shutdown() {
        if (function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        }
    }

    /**
     * Sets up the request object
     */
    protected function setupRequest() {
        $this->request = HttpRequest::createFromGlobals();

        return $this;
    }

    /**
     * Get a route for a request
     * @param \MD\Framework\Request\HttpRequest $request A request object
     * @return Route|null
     * 
     * @todo Handle patterns
     */
    protected function getRoute(HttpRequest $request = null) {
        if (is_null($request)) {
            return null;
        }

        foreach ($this->routes as $route) {
            if ($route->getUriPattern() == $request->getRequestUri()) {
                return $route;
            }
        }

        return null;
    }

    /**
     * Runs the app
     * @throws \LogicException
     */
    public function run() {
        ErrorHandler::register();
        $this->setupRoutes();
        $this->setupRequest();

        $route = $this->getRoute($this->request);

        if (!is_null($route)) {
            $response = call_user_func($route->getCallback(), $this);
        } else {
            $response = new \MD\Framework\Response\HttpNotFoundResponse('No route matches: ' . $this->request->getRequestUri());
        }

        if (!($response instanceof Response\HttpResponse)) {
            throw new \LogicException('Invalid response');
        }

        $response->sendHeaders();
        $response->sendContent();

        $this->shutdown();
    }

}
