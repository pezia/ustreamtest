<?php

namespace MD\Framework\Config;

/**
 * Array config loader
 * Loads config values from a series of files in a given directory.
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class ArrayLoader implements ILoader {

    /**
     * Cached content
     * @var array
     */
    private $cache = array();

    /**
     * Configuration base directory
     * @var string 
     */
    protected $configDirectory;

    /**
     * Construcor
     * 
     * @param string $configDirectory Configuration base directory
     */
    public function __construct($configDirectory) {
        $this->configDirectory = $configDirectory;
    }

    /**
     * @inheritdoc
     */
    public function load($key, $default = null) {
        return $default;
    }

}
