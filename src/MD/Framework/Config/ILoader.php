<?php

namespace MD\Framework\Config;

/**
 * Config loader interface
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
interface ILoader {

    /**
     * Load a config value based on a key
     * @param string $key The key to load wich may be a dot separated structure
     * @param mixed $default The defautl value when the key was not found
     * 
     * @return mixed The value of the key or the default when not found
     */
    public function load($key, $default = null);
}
