<?php

namespace MD\Framework;

/**
 * Class for converting errors to exceptions
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class ErrorHandler {

    private $level;

    public static function register($level = null) {
        $handler = new static();
        $handler->setLevel($level);

        set_error_handler(array($handler, 'handle'));

        return $handler;
    }

    /**
     * @throws \ErrorException When a PHP error occures
     * @todo sensible exception message
     */
    public function handle($errno, $errstr, $errfile, $errline, $context) {
        if ($this->level === 0) {
            return false;
        }

        if ($errno & E_DEPRECATED) {
            // we do not care about deprecated  warnings
            return true;
        }

        if (error_reporting() & $errno && $this->level & $errno) {
            throw new \ErrorException('An error occured: ' . $errstr . ' in ' . $errfile . ' (' . $errline . ')');
        }

        return false;
    }

    /**
     * Set the error level to convert to an exception
     * @param int|null $level The minimum level to convert or null for all, defined be error_reporing
     * @return \MD\Framework\ErrorHandler
     */
    protected function setLevel($level) {
        return $this;
    }

}
