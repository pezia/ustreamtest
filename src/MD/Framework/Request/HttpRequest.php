<?php

namespace MD\Framework\Request;

/**
 * Class for handling HTTP requests
 * 
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class HttpRequest {

    const METHOD_DELETE = 'DELETE';
    const METHOD_GET = 'GET';
    const METHOD_HEAD = 'HEAD';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';

    /**
     * @var arrray GET parameters
     */
    protected $query;

    /**
     * @var array POST parameters 
     */
    protected $request;

    /**
     * @var array Posted files
     */
    protected $files;

    /**
     * @var array Cookies
     */
    protected $cookies;

    /**
     * @var array Server variables
     */
    protected $server;

    /**
     * @var string Raw content (unused) 
     */
    protected $content;

    /**
     * @var string HTTP method
     */
    protected $method = null;

    /**
     * @var string Request URI
     */
    protected $requestUri = null;

    /**
     * Constructor
     * 
     * @param string $method HTTP request method
     * @param array $getParams
     * @param array $postParams
     * @param array $files
     * @param string $content
     */
    public function __construct($query = array(), $request = array(), $files = array(), $cookies = array(), $server = array(), $content = '') {
        $this->query = $query;
        $this->request = $request;
        $this->files = $files;
        $this->cookies = $cookies;
        $this->server = $server;
        $this->content = $content;
    }

    /**
     * Factory method to create a request from the PHP superglobals
     * 
     * @return MD\Framework\Request\HttpRequest
     */
    public static function createFromGlobals() {
        return new static($_GET, $_POST, $_FILES, $_COOKIE, $_SERVER);
    }

    // <editor-fold desc="Getters" defaultstate="collapsed">

    /**
     * Get the server array
     * @return array Server array
     */
    public function getServer() {
        return $this->server;
    }

    /**
     * Get the query (GET) array
     * @return array Query array
     */
    public function getQuery() {
        return $this->query;
    }

    /**
     * Get the request (POST) array
     * @return array Request array
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Get the cookies
     * @return array Cookies array
     */
    public function getCookies() {
        return $this->cookies;
    }

    /**
     * Get the HTTP method
     * @return string HTTP method
     * 
     * @todo Handle PUT and DELETE through X-HTTP-METHOD-OVERRIDE
     */
    public function getMethod() {
        if (is_null($this->method)) {
            $this->method = strtoupper(isset($this->server['REQUEST_METHOD']) ? $this->server['REQUEST_METHOD'] : 'GET');
        }

        return $this->method;
    }

    /**
     * Checks if the request method is the same as the given one.
     *
     * @param string $method HTTP method
     *
     * @return boolean
     */
    public function isMethod($method) {
        return $this->getMethod() === strtoupper($method);
    }

    /**
     * Returns if the request was requested with XMLHttpRequest.
     * This is based on the request headers
     * 
     * @return boolean The request was requested with XMLHttpRequest
     */
    public function isXmlHttpRequest() {
        return isset($this->server['HTTP_X-Requested-With']) ? $this->server['HTTP_X-Requested-With'] === 'XMLHttpRequest' : false;
    }

    /**
     * Get the request URI
     * @return string|false False, when it could not be detected
     * 
     * @todo Make it reliable
     */
    public function getRequestUri() {
        $baseUrl = isset($this->server['SCRIPT_NAME']) ? $this->server['SCRIPT_NAME'] : '';

        if (isset($this->server['REQUEST_URI']) && strpos($this->server['REQUEST_URI'], $baseUrl) === false) {
            $baseUrl = rtrim(dirname($baseUrl), '/\\');
        }

        return '/' . ltrim(isset($this->server['REQUEST_URI']) ? substr($this->server['REQUEST_URI'], strlen($baseUrl)) : '', '/');
    }

    // </editor-fold>
}