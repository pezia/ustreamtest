<?php

namespace MD\Framework\Response;

/**
 * Class for handling Not found events
 * 
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class HttpNotFoundResponse extends HttpResponse {

    public function __construct($content = '') {
        parent::__construct($content, 404);
    }

}
