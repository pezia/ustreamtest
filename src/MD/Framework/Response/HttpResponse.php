<?php

namespace MD\Framework\Response;

/**
 * Class for handling HTTP responses
 * It is as simple as possible
 * 
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class HttpResponse {

    /**
     * Status codes translation table
     * Stolen from symfony
     * (c) Fabien Potencier <fabien@symfony.com>
     *
     * @var array
     */
    public static $statusTexts = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing', // RFC2518
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status', // RFC4918
        208 => 'Already Reported', // RFC5842
        226 => 'IM Used', // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Reserved',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect', // RFC-reschke-http-status-308-07
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot', // RFC2324
        422 => 'Unprocessable Entity', // RFC4918
        423 => 'Locked', // RFC4918
        424 => 'Failed Dependency', // RFC4918
        425 => 'Reserved for WebDAV advanced collections expired proposal', // RFC2817
        426 => 'Upgrade Required', // RFC2817
        428 => 'Precondition Required', // RFC6585
        429 => 'Too Many Requests', // RFC6585
        431 => 'Request Header Fields Too Large', // RFC6585
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates (Experimental)', // RFC2295
        507 => 'Insufficient Storage', // RFC4918
        508 => 'Loop Detected', // RFC5842
        510 => 'Not Extended', // RFC2774
        511 => 'Network Authentication Required', // RFC6585
    );

    /**
     * @var string The response content
     */
    protected $content;

    /**
     * @var int Response HTTP status code
     */
    protected $statusCode;

    /**
     * @var array Array of HTTP headers
     */
    protected $headers;

    /**
     * Contructor
     * 
     * @param string $content Response content
     * @param int    $statusCode HTTP status code
     * @param array  $headers Array of HTTP headers
     */
    public function __construct($content = '', $statusCode = 200, array $headers = array()) {
        $this->content = $content;
        $this->setStatusCode($statusCode);
        $this->headers = $headers;
    }

    /**
     * Factory method for chainability and for dynamic instances
     *
     * @param string  $content Response content
     * @param integer $statusCode HTTP status code
     * @param array   $headers Array of HTTP headers
     *
     * @return \MD\Framework\Response\HttpResponse
     */
    public static function create($content = '', $statusCode = 200, $headers = array()) {
        return new static($content, $statusCode, $headers);
    }

    /**
     * Sends the headers and the content
     * 
     * @return \MD\Framework\Response\HttpResponse
     */
    public function send() {
        $this->sendHeaders()->sendContent();

        return $this;
    }

    /**
     * Sends the headers
     * 
     * @return \MD\Framework\Response\HttpResponse
     */
    public function sendHeaders() {
        header(sprintf('HTTP/1.0 %d %s', $this->statusCode, isset(self::$statusTexts[$this->statusCode]) ? self::$statusTexts[$this->statusCode] : '' ));
        foreach ($this->headers as $header => $value) {
            header($header . ': ' . $value);
        }

        return $this;
    }

    /**
     * Sends the content
     * 
     * @return \MD\Framework\Response\HttpResponse
     */
    public function sendContent() {
        echo $this->content;

        return $this;
    }

    // <editor-fold desc="Getters, setters" defaultstate="collapsed">

    /**
     * Get the current content
     * 
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set the current content
     * 
     * @param string $content The content
     * @return \MD\Framework\Response\HttpResponse
     */
    public function setContent($content) {
        $this->content = (string) $content;
        return $this;
    }

    /**
     * Get the headers array
     * @return array Array of HTTP headers
     */
    public function getHeaders() {
        return $this->headers;
    }

    /**
     * Set the header array
     * 
     * @param array $headers Array of HTTP headers
     * @return \MD\Framework\Response\HttpResponseS
     */
    public function setHeaders(array $headers) {
        $this->headers = $headers;
        return $this;
    }

    /**
     * Clears the HTTP headers array
     * @return \MD\Framework\Response\HttpResponse
     */
    public function clearHeaders() {
        $this->setHeaders(array());
        return $this;
    }

    /**
     * Get the current status code
     * @return int The current HTTP status code
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * Set the current status code
     * 
     * @param int $statusCode An HTTP status code
     * @return \MD\Framework\Response\HttpResponseS
     */
    public function setStatusCode($statusCode) {
        $this->statusCode = (int) $statusCode;
        return $this;
    }

    // </editor-fold>
}
