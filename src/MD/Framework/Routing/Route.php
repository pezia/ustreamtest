<?php

namespace MD\Framework\Routing;

/**
 * Route class
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
final class Route {
    /**
     * @var string URI pattern
     */
    protected $uriPattern;
    
    /**
     * @var callback Callback
     */
    protected $callback;
    
    /**
     * Constructor
     * @param string $uriPattern URI pattern
     * @param callable $callback A callable to execute if this route matches
     */
    public function __construct($uriPattern, callable $callback) {
        $this->uriPattern = $uriPattern;
        $this->callback = $callback;
    }
    
    // <editor-fold desc="Getters" defaultstate="collapsed">
    
    /**
     * Get the URI pattern
     * @return string
     */
    public function getUriPattern() {
        return $this->uriPattern;
    }
    
    /**
     * Get the associated the callback
     * @return callback
     */
    public function getCallback() {
        return $this->callback;
    }
    // </editor-fold>
}
