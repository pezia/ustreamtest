<?php

namespace MD\Framework\Session\Handler;

/**
 * Memcached based session handler
 * Requires the PHP Memcached extension (with a D)
 *
 * @see http://php.net/memcached
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class Memcached implements \SessionHandlerInterface {

    /**
     * @var \Memcached Memcached instance
     */
    private $memcached;

    /**
     * @var integer Session lifetime
     */
    private $lifetime;

    /**
     * @var string Key prefix
     */
    private $keyPrefix;

    /**
     * Constructor
     * Available options: lifetime, prefix
     *
     * @param \Memcached $memcached A \Memcached instance
     */
    public function __construct(\Memcached $memcached, array $options = array()) {
        $this->memcached = $memcached;

        $this->lifetime = isset($options['lifetime']) ? $options['lifetime'] : null;
        $this->keyPrefix = isset($options['prefix']) ? $options['prefix'] : null;
    }

    /**
     * Close handler
     */
    public function close() {
        return true;
    }

    /**
     * Destroy handler
     */
    public function destroy($sessionId) {
        return $this->memcached->delete($this->keyPrefix . $sessionId);
    }

    /**
     * GC handler
     */
    public function gc($lifetime) {
        return true;
    }

    /**
     * Open hander
     */
    public function open($savePath, $sessionName) {
        return true;
    }

    /**
     * Read handler
     */
    public function read($sessionId) {
        return $this->memcached->get($this->keyPrefix . $sessionId) ? : '';
    }

    /**
     * Write handler
     */
    public function write($sessionId, $data) {
        return $this->memcached->set($this->keyPrefix . $sessionId, $data, $this->lifetime ? time() + $this->lifetime : null);
    }

}
