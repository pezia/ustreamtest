<?php

namespace MD\Framework\Session;

/**
 * Session interface
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
interface ISession {

    /**
     * Constructor
     * @param string $sessionId Session ID
     */
    public function __construct($sessionId = null);

    /**
     * Starts the session.
     */
    public function start($readOnly = false);

    /**
     * Get the current session ID.
     *
     * @return string Session ID.
     */
    public function getId();

    /**
     * Set the session ID
     *
     * @param string $id
     */
    public function setId($id);

    /**
     * Destroys the current session.
     */
    public function destroy();

    /**
     * Regenerate session ID
     */
    public function regenerate();

    /**
     * Force the session to be saved and closed.
     */
    public function close();

    /**
     * Session shutdown method
     * Called at the end of the execution
     */
    public function shutdown();

    /**
     * Get a session variable
     * @param string $key
     */
    public function get($key);

    /**
     * Set a session variable
     * @param strine $key
     * @param mixes $value
     */
    public function set($key, $value);
}
