<?php

namespace MD\Framework\Session;

/**
 * Session class
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class Session implements ISession {

    public function __construct($sessionId = null) {
        
    }

    public function close() {
        
    }

    public function destroy() {
        
    }

    public function get($key) {
        
    }

    public function getId() {
        
    }

    public function regenerate() {
        
    }

    public function set($key, $value) {
        
    }

    public function setId($id) {
        
    }

    public function shutdown() {
        
    }

    public function start($readOnly = false) {
        
    }

}
