<?php

namespace MD\Framework\Tests;

class MockApp extends \MD\Framework\App {
    protected function setupRoutes() {
        $this->routes['welcome'] = new \MD\Framework\Routing\Route('/', function () {
        });
        
        $this->routes['route_1'] = new \MD\Framework\Routing\Route('/one', function () {
        });
    }
}

/**
 * App Tests
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class AppTest extends \PHPUnit_Framework_TestCase {
    
    public function testClass() {
        $app = new MockApp();
    }
    
    public function testRoutes() {
        
    }
}
