<?php

namespace MD\Framework\Tests\Request;

use \MD\Framework\Request\HttpRequest;

/**
 * HttpRequest Tests
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class HttpRequestTest extends \PHPUnit_Framework_TestCase {

    public function testClassExists() {
        $this->assertTrue(class_exists('\MD\Framework\Request\HttpRequest'));
        $request = new HttpRequest();
    }
    
    /**
     * @depends testClassExists
     * @todo Check $_FILES aswell
     */
    public function testCreateFromGlobals() {
        $_GET = array(
            'get_param' => 'param',
            'get_param_2' => 'param2',
            'embedded' => array(
                'things' => 'thing',
            ),
        );
        
        $_POST = array(
            'post_param' => 'post_param',
            'post_param_2' => 'post_param2',
            'embedded_post' => array(
                'poster_things' => 'thing_post',
            ),
        );
        
        $_COOKIE = array(
            'mycookie' => 'did not worth it',
        );
        
        $request = HttpRequest::createFromGlobals();
        
        $this->assertEquals($_SERVER, $request->getServer(), 'Server is not the same');
        $this->assertEquals($_GET, $request->getQuery(), 'GET parameters are not the same');
        $this->assertEquals($_POST, $request->getRequest(), 'POST parameters are not the same');
        $this->assertEquals($_COOKIE, $request->getCookies(), 'Cookies are not the same');
        $this->assertFalse($request->getRequestUri(), 'Wrong request URI');
        $this->assertFalse($request->isXmlHttpRequest(), 'Is an "AJAX" request');
        $this->assertTrue($request->isMethod('GET'), 'Is not a GET');
        $this->assertFalse($request->isMethod('POST'), 'Is a POST');
        
        $_SERVER['REQUEST_URI'] = 'foo/bar/baz';
        $_SERVER['HTTP_X-Requested-With'] = 'XMLHttpRequest';
        
        $request2 = HttpRequest::createFromGlobals();
        
        $this->assertSame('foo/bar/baz', $request2->getRequestUri(), 'Wrong request URI');
        $this->assertTrue($request2->isXmlHttpRequest(), 'Is not an "AJAX" request');
    }

}
