<?php

namespace MD\Framework\Tests\Routing;

use \MD\Framework\Routing\Route;

/**
 * Route Tests
 *
 * @author Zsolt Petrik <petrik.zsolt@marbledigital.eu>
 */
class RouteTest extends \PHPUnit_Framework_TestCase {
    public function routeDataProvider() {
        return array(
            'route_foo' => array('uriPattern' => '/foo', 'callback' => (function () { return 'foo';}), 'expected' => 'foo'),
            'route_bar' => array('uriPattern' => '/bar', 'callback' => (function () { return 'bar';}), 'expected' => 'bar'),
            'route_baz' => array('uriPattern' => '/baz', 'callback' => (function () { return 'baz';}), 'expected' => 'baz'),
        );
    }
    
    public function testClassExists() {
        $this->assertTrue(class_exists('\MD\Framework\Routing\Route'));
        $route = new Route('uri', function () {});
    }
    
    /**
     * @depends testClassExists
     * @dataProvider routeDataProvider
     */
    public function testRoute($uriPattern, $callback, $expected) {
        $route = new Route($uriPattern, $callback);
        $this->assertSame($uriPattern, $route->getUriPattern(), 'Invalid URI pattern');
        $this->assertTrue(is_callable($route->getCallback()), 'Callback s not a callable');
        $this->assertSame($expected, call_user_func($route->getCallback()), 'Invalid callback');
    }
}
